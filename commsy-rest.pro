TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
        qcommsy/curlreq.cpp \
        qcommsy/libcommsy.cpp

INCLUDEPATH += /usr/include/jsoncpp
LIBS += -ldrogon -ltrantor -luuid -lz -lcrypto -ldl -ljson-c -lsqlite3 -lssl -ljsoncpp -pthread
LIBS += -lgumbo -lcurl -lcurlpp

HEADERS += \
    qcommsy/libcommsy.hpp
