### Commsy REST API
Provides a REST API to commsy servers via libcommsy

## Example server
`://...//`
No longer up and running 

Endpoints:

# `fetchList`
GET parameters:
 * url: URL to the commsy server (start page)
 * sid: SID to authenticate with
 * room: room to load
 * startId: ID before the first ID to list (optional, default: start with first post)
 * maxPosts: maximum amount of posts to load

 # `fetchDescription`
GET parameters:
 * url: URL to the commsy post
 * sid: SID to authenticate with
